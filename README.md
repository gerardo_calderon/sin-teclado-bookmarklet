#Sin Teclado

Bookmarklet para eliminar el teclado virtual de la Banca en línea del Banco Promérica El Salvador.

La ejecución completamente local sin inclusión de archivos remotos.

Script principal del bookmarklet está en `res/inc/script.js`

##Licencia
[GNU GPL v3](http://www.gnu.org/licenses/gpl-3.0.txt)